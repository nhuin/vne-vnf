# VNE-VNF algorithms

This repository contains algorithms for the VNE-VNF problem, as defined in [An Approach to Network Service Placement using Intelligent Search Strategies over Branch-and-Bound](https://doi.org/10.1109/GLOBECOM46510.2021.9685796).
Currently, we provide
* a compact ILP model,
* a column generation model,
* randomized and branch and bound resolution from the CG model.

# Building the project

## Building requirements

* CMake 3.16+
* Conan 2

## Library requirements

* Gurobi 11
* CppRO 0.7

# Docker image

The repository provides a Dockerfile that you can use to build and build the image.
It relies on the [Gurobi docker image](https://hub.docker.com/r/gurobi/optimizer) and can run if you provide a [WSL Gurobi](https://www.gurobi.com/features/web-license-service/) license file

Once you build the container, (e.g., using `docker . build -t vne_vnf`), you can run the ILP algorithm using the following command:

```bash
docker run -it -v./data:/data -v /home/nhuin/gurobi-wsl.lic:/opt/gurobi/gurobi.lic vne_vnf /build/src/VNE_VNF /data/Abvt_daisy_3.json
```


