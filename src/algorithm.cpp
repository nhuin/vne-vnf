#include "algorithm.hpp"
#include "models.hpp"
#include "utils.hpp"
#include <gurobi_c++.h>
#include <gurobi_c.h>
#include <optional>
#include <random>
#include <spdlog/spdlog.h>

namespace VNE {

VNE::ExtendedFormulation columnGeneration(const Instance& _inst, GRBEnv& _env) {
    VNE::ExtendedFormulation rmp(_inst, _env);
    rmp.getModel().set(GRB_IntParam_OutputFlag, 0);
    std::size_t nbIterations = 0;
    std::size_t nbGeneratedColumns = 0;
    const auto masterFunction = [&rmp, &nbGeneratedColumns, &_inst, &nbIterations](
                                  const std::vector<Embedding>& _embeddings)
      -> std::optional<ExtendedFormulation::DualValues> {
        for (auto& emb : _embeddings) {
            rmp.addColumn(emb, _inst);
            nbGeneratedColumns++;
        }
        rmp.getModel().write("RMP.lp");
        rmp.getModel().optimize();
        if (rmp.getModel().get(GRB_IntAttr_Status) == GRB_INFEASIBLE) {
            spdlog::error("RMP is infeasible");
            return std::nullopt;
        }
        spdlog::info("\r# Ite: {}, Objective Value: {:.6f}, # cols : {}",
          ++nbIterations,
          rmp.getModel().getObjective().getValue(),
          nbGeneratedColumns);
        return rmp.getDualValues();
    };
    // Define pricings function
    std::vector<EmbeddingILP> pricings;
    pricings.reserve(_inst.getServices().size());
    std::transform(_inst.getServices().begin(),
      _inst.getServices().end(),
      std::back_inserter(pricings),
      [&](const Service& _service) { return EmbeddingILP(_inst, _service, _env); });
    for (auto& pp : pricings) {
        pp.getModel().update();
    }
    std::vector<Embedding> columns;
    const auto pricingFunction = [&columns, &_inst, &pricings](
                                   const VNE::ExtendedFormulation::DualValues& _duals)
      -> std::optional<std::vector<Embedding>> {
        columns.clear();
        for (std::size_t k = 0; k < _inst.getServices().size(); ++k) {
            pricings[k].setServiceDelay(_inst.getServices()[k].serviceDelay);
            pricings[k].setObjectiveCoefficient(
              _duals.linkCapacityDuals, _duals.cpuCapacityDuals, _duals.storageCapacityDuals);
            pricings[k].getModel().set(GRB_IntParam_OutputFlag, 0);
            pricings[k].getModel().write("PP.lp");
            pricings[k].getModel().optimize();
            if (pricings[k].getModel().get(GRB_IntAttr_Status) == 3) {
                spdlog::error("PP is infeasible");
                continue;
            }
            for (const auto& col : pricings[k].getEmbeddings(k)) {
                if (epsilon_less<>()(0, getReducedCost(_duals, col, _inst))) {
                    columns.emplace_back(std::move(col));
                }
            }
        }
        if (columns.empty()) {
            return std::nullopt;
        }
        return columns;
    };
    columnGeneration(masterFunction, pricingFunction);
    return rmp;
}

std::pair<std::optional<Solution>, double> cgAndBranchAndBound(const Instance& _inst,
  GRBEnv& _env) {
    spdlog::info("Solving RMP");
    auto rmp = columnGeneration(_inst, _env);
    auto upperBound = rmp.getModel().get(GRB_DoubleAttr_ObjBound);
    spdlog::info("Setting model to integer");
    rmp.setColumnType(GRB_INTEGER);
    rmp.getModel().set(GRB_IntParam_OutputFlag, 1);
    spdlog::info("Solving integer model");
    rmp.getModel().optimize();
    return { rmp.buildSolution(_inst), upperBound };
}

std::pair<std::optional<Solution>, double>
  cgAndRandomizedRounding(const Instance& _inst, GRBEnv& _env, std::mt19937& _gen) {
    auto rmp = columnGeneration(_inst, _env);
    auto upperBound = rmp.getModel().get(GRB_DoubleAttr_ObjBound);
    std::size_t bestSol = 0;
    for (std::size_t i = 0; i < 100; ++i) {
        const auto threshold = std::uniform_real_distribution<double>(0.0, 1.0)(_gen);
        std::size_t nbServices = 0;
        for (const auto& services : rmp.getColumns()) {
            for (const auto& col : services) {
                const auto val = col.var.get(GRB_DoubleAttr_X);
                const double decimalPart = val - std::floor(val);
                if (decimalPart > threshold) {
                    nbServices++;
                }
            }
        }
        bestSol = std::max(bestSol, nbServices);
    }
    spdlog::info("Accepted {} services", bestSol);
    return { std::nullopt, upperBound };
}

}// namespace VNE
