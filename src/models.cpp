#include "models.hpp"
#include <cmath>
#include <complex>
#include <fmt/format.h>
#include <gurobi_c++.h>
#include <iterator>
#include <numeric>
#include <utility>
#include <vector>
#include <boost/graph/graph_utility.hpp>
#include <boost/range/numeric.hpp>
#include "utils.hpp"
#include "vne.hpp"
#include <spdlog/spdlog.h>


namespace VNE {

CompactILP::CompactILP(const Instance& _inst, GRBEnv& _env)
  : m_model(_env)
  , m_flowVars([&] {
      std::vector<std::vector<std::vector<std::vector<GRBVar>>>> retval(_inst.getNbServices());
      for (const auto& [k, service] : enumerate(_inst.getServices())) {
          retval[k].resize(service.nbRequested,
            { num_edges(_inst.getPhysicalNetwork()),
              std::vector<GRBVar>(num_edges(service.network)) });
          for (std::size_t i = 0; i < service.nbRequested; ++i) {
              for (std::size_t e = 0; e < num_edges(_inst.getPhysicalNetwork()); ++e) {
                  for (std::size_t l = 0; l < num_edges(service.network); ++l) {
                      retval[k][i][e][l] = m_model.addVar(0.0,
                        GRB_INFINITY,
                        0.0,
                        GRB_BINARY,
                        fmt::format("flow(e~{})(i~{})(k~{})(l~{})", e, i, k, l));
                  }
              }
          }
      }
      return retval;
  }())
  , m_vnfVars([&] {
      std::vector<std::vector<std::vector<std::vector<GRBVar>>>> retval(_inst.getNbServices());
      for (const auto& [k, service] : enumerate(_inst.getServices())) {
          retval[k].resize(service.nbRequested,
            { num_vertices(service.network),
              std::vector<GRBVar>(num_vertices(_inst.getPhysicalNetwork())) });
          for (std::size_t i = 0; i < service.nbRequested; ++i) {
              for (std::size_t f = 0; f < num_vertices(service.network); ++f) {
                  if (service.fixedLocations[f] == Service::NoFixedLocation) {
                      for (std::size_t v = 0; v < num_vertices(_inst.getPhysicalNetwork()); ++v) {
                          retval[k][i][f][v] = m_model.addVar(0.0,
                            GRB_INFINITY,
                            f == 0 ? 1.0 : 0.0,
                            GRB_BINARY,
                            fmt::format("vnf(k~{})(i~{})(f~{})(v~{})", k, i, v, f));
                      }
                  } else {
                      for (std::size_t v = 0; v < num_vertices(_inst.getPhysicalNetwork()); ++v) {
                          retval[k][i][f][v] = m_model.addVar(0.0,
                            0.0,
                            f == 0 ? 1.0 : 0.0,
                            GRB_BINARY,
                            fmt::format("vnf(k~{})(i~{})(f~{})(v~{})", k, i, v, f));
                      }
                      for (const auto v : service.fixedLocations[f]) {
                          retval[k][i][f][v].set(GRB_DoubleAttr_UB, 1.0);
                      }
                  }
              }
          }
      }
      return retval;
  }())
  , m_vnfToGraphConstraints([&] {
      std::vector<std::vector<std::vector<GRBConstr>>> retval(_inst.getNbServices());
      for (const auto& [k, service] : enumerate(_inst.getServices())) {
          retval[k].resize(
            service.nbRequested, std::vector<GRBConstr>(num_vertices(service.network)));
          for (std::size_t i = 0; i < service.nbRequested; ++i) {
              for (std::size_t f = 0; f < num_vertices(service.network); ++f) {
                  GRBLinExpr expr;
                  for (std::size_t v = 0; v < num_vertices(_inst.getPhysicalNetwork()); ++v) {
                      expr += m_vnfVars[k][i][f][v];
                  }
                  retval[k][i][f] = m_model.addConstr(
                    expr <= 1, fmt::format("vnfToGraph(k~{})(i~{})(f~{})", k, i, f));
              }
          }
      }
      return retval;
  }())
  , m_oneFunctionPerNodeConstraints([&] {
      std::vector<std::vector<std::vector<GRBConstr>>> retval(_inst.getNbServices());
      for (const auto& [k, service] : enumerate(_inst.getServices())) {
          retval[k].resize(
            service.nbRequested, std::vector<GRBConstr>(num_vertices(_inst.getPhysicalNetwork())));
          for (std::size_t i = 0; i < service.nbRequested; ++i) {
              for (std::size_t v = 0; v < num_vertices(_inst.getPhysicalNetwork()); ++v) {
                  GRBLinExpr expr;
                  for (std::size_t f = 0; f < num_vertices(service.network); ++f) {
                      expr += m_vnfVars[k][i][f][v];
                  }
                  retval[k][i][v] = m_model.addConstr(
                    expr <= 1, fmt::format("oneFunctionPerNode(k~{})(i~{})(v~{})", k, i, v));
              }
          }
      }
      return retval;
  }())
  , m_flowConservationConstraints([&] {
      std::vector<std::vector<std::vector<std::vector<GRBConstr>>>> retval(_inst.getNbServices());
      for (const auto& [k, service] : enumerate(_inst.getServices())) {
          retval[k].resize(service.nbRequested,
            { num_vertices(_inst.getPhysicalNetwork()),
              std::vector<GRBConstr>(num_edges(service.network)) });
          for (std::size_t i = 0; i < service.nbRequested; ++i) {
              for (std::size_t v = 0; v < num_vertices(_inst.getPhysicalNetwork()); ++v) {
                  for (const auto ved : boost::make_iterator_range(edges(service.network))) {
                      const auto l = service.network[ved].id;
                      GRBLinExpr expr;
                      for (const auto& ped :
                        boost::make_iterator_range(out_edges(v, _inst.getPhysicalNetwork()))) {
                          const auto e = _inst.getPhysicalNetwork()[ped].id;
                          expr += m_flowVars[k][i][e][l];
                      }
                      for (const auto& ped :
                        boost::make_iterator_range(in_edges(v, _inst.getPhysicalNetwork()))) {
                          const auto e = _inst.getPhysicalNetwork()[ped].id;
                          expr -= m_flowVars[k][i][e][l];
                      }
                      const auto& [f1, f2] = incident(ved, service.network);
                      expr += -m_vnfVars[k][i][f1][v] + m_vnfVars[k][i][f2][v];
                      retval[k][i][v][l] = m_model.addConstr(expr == 0,
                        fmt::format("flowConservation(k~{})(i~{})(v~{})(l~{})", k, i, v, l));
                  }
              }
          }
      }
      return retval;
  }())
  , m_linkCapacityConstraints([&] {
      std::vector<GRBConstr> retval(num_edges(_inst.getPhysicalNetwork()));
      for (const auto& ped : boost::make_iterator_range(edges(_inst.getPhysicalNetwork()))) {
          const auto& e = _inst.getPhysicalNetwork()[ped].id;
          GRBLinExpr expr;
          for (const auto& [k, service] : enumerate(_inst.getServices())) {
              for (std::size_t i = 0; i < service.nbRequested; ++i) {
                  for (std::size_t l = 0; l < num_edges(service.network); ++l) {
                      expr += service.getBandwidthRequirements()[l] * m_flowVars[k][i][e][l];
                  }
              }
          }
          retval[e] = m_model.addConstr(
            expr <= _inst.getLinkCapacities()[e], fmt::format("linkCapacity(e~{})", e));
      }
      return retval;
  }())
  , m_cpuCapacityConstraints([&] {
      std::vector<GRBConstr> retval(num_vertices(_inst.getPhysicalNetwork()));
      for (std::size_t v = 0; v < num_vertices(_inst.getPhysicalNetwork()); ++v) {
          GRBLinExpr expr;
          for (const auto& [k, service] : enumerate(_inst.getServices())) {
              for (std::size_t i = 0; i < service.nbRequested; ++i) {
                  for (std::size_t f = 0; f < num_vertices(service.network); ++f) {
                      expr += service.getCpuRequirements()[f] * m_vnfVars[k][i][f][v];
                  }
              }
          }
          retval[v] = m_model.addConstr(
            expr <= _inst.getCPUCapacities()[v], fmt::format("nodeCapa(v~{})", v));
      }
      return retval;
  }())
  , m_storageCapacityConstraints([&] {
      std::vector<GRBConstr> retval(num_vertices(_inst.getPhysicalNetwork()));
      for (std::size_t v = 0; v < num_vertices(_inst.getPhysicalNetwork()); ++v) {
          GRBLinExpr expr;
          for (const auto& [k, service] : enumerate(_inst.getServices())) {
              for (std::size_t i = 0; i < service.nbRequested; ++i) {
                  for (std::size_t f = 0; f < num_vertices(service.network); ++f) {
                      expr += service.getStorageRequirements()[f] * m_vnfVars[k][i][f][v];
                  }
              }
          }
          retval[v] = m_model.addConstr(
            expr <= _inst.getStorageCapacities()[v], fmt::format("nodeCapa(v~{})", v));
      }
      return retval;
  }())
  , m_serviceDelayConstraints([&] {
      std::vector<std::vector<GRBConstr>> retval(_inst.getNbServices());
      for (const auto& [k, service] : enumerate(_inst.getServices())) {
          retval[k].resize(service.nbRequested);
          for (std::size_t i = 0; i < service.nbRequested; ++i) {
              GRBLinExpr expr;
              for (std::size_t e = 0; e < num_edges(_inst.getPhysicalNetwork()); ++e) {
                  for (std::size_t l = 0; l < num_edges(service.network); ++l) {
                      expr += _inst.getLinkDelays()[e] * m_flowVars[k][i][e][l];
                  }
              }
              retval[k][i] = m_model.addConstr(
                expr <= service.serviceDelay, fmt::format("serviceDelay(k~{})(i~{})", k, i));
          }
      }
      return retval;
  }())
  , m_vnfDelayConstraints([&] {
      std::vector<std::vector<std::vector<GRBConstr>>> retval(_inst.getNbServices());
      for (const auto& [k, service] : enumerate(_inst.getServices())) {
          retval[k].resize(service.nbRequested, std::vector<GRBConstr>(num_edges(service.network)));
          for (std::size_t i = 0; i < service.nbRequested; ++i) {
              for (std::size_t l = 0; l < num_edges(service.network); ++l) {
                  GRBLinExpr expr;
                  for (std::size_t e = 0; e < num_edges(_inst.getPhysicalNetwork()); ++e) {
                      expr += _inst.getLinkDelays()[e] * m_flowVars[k][i][e][l];
                  }
                  retval[k][i][l] = m_model.addConstr(expr <= service.getDelayRequirements()[l],
                    fmt::format("vnfDelay(k~{})(i~{})(l~{})", k, i, l));
              }
          }
      }
      return retval;
  }())
  , m_maxBandwidthCut([&] {
      GRBLinExpr expr;
      for (const auto& [k, service] : enumerate(_inst.getServices())) {
          for (std::size_t i = 0; i < service.nbRequested; ++i) {
              double minServiceBw = 0.0;
              for (std::size_t l = 0; l < num_edges(service.network); ++l) {
                  minServiceBw += service.getBandwidthRequirements()[l];
              }
              for (std::size_t v = 0; v < num_vertices(_inst.getPhysicalNetwork()); ++v) {
                  expr += minServiceBw * m_vnfVars[k][i][0][v];
              }
          }
      }
      const double totalLinkCapacity =
        std::accumulate(_inst.getLinkCapacities().begin(), _inst.getLinkCapacities().end(), 0.0);
      return m_model.addConstr(expr <= totalLinkCapacity, "bwCut");
  }())

{
    m_model.set(GRB_IntAttr_ModelSense, GRB_MAXIMIZE);
}

void CompactILP::setVarType(char _type) {
    for (auto& varss : m_flowVars) {
        for (auto& vars : varss) {
            for (auto& var : vars) {
                for (auto& v : var) {
                    v.set(GRB_CharAttr_VType, _type);
                }
            }
        }
    }

    for (auto& varss : m_vnfVars) {
        for (auto& vars : varss) {
            for (auto& var : vars) {
                for (auto& v : var) {
                    v.set(GRB_CharAttr_VType, _type);
                }
            }
        }
    }
}

std::optional<Solution> CompactILP::buildSolution(const Instance& _inst) const {
    if (getModel().get(GRB_IntAttr_Status) == GRB_INFEASIBLE
        || m_flowVars[0][0][0][0].get(GRB_CharAttr_VType) == GRB_CONTINUOUS) {
        return std::nullopt;
    }

    Solution sol;
    for (const auto& [k, service] : enumerate(_inst.getServices())) {
        for (std::size_t i = 0; i < service.nbRequested; ++i) {
            auto emb = [&, &k = k, &service = service]() -> std::optional<Embedding> {
                Embedding retval;
                retval.serviceId = k;
                retval.vertices.resize(
                  num_vertices(service.network), std::numeric_limits<std::size_t>::max());
                for (std::size_t f = 0; f < num_vertices(service.network); ++f) {
                    for (std::size_t v = 0; v < num_vertices(_inst.getPhysicalNetwork()); ++v) {
                        if (m_vnfVars[k][i][f][v].get(GRB_DoubleAttr_X) > 0.5) {
                            retval.vertices[f] = v;
                            break;
                        }
                    }
                    if (retval.vertices[f] == std::numeric_limits<std::size_t>::max()) {
                        return std::nullopt;
                    }
                }
                retval.edges.resize(num_edges(service.network));
                for (std::size_t l = 0; l < num_edges(service.network); ++l) {
                    for (const auto ped :
                      boost::make_iterator_range(edges(_inst.getPhysicalNetwork()))) {
                        const auto e = _inst.getPhysicalNetwork()[ped].id;
                        if (m_flowVars[k][i][e][l].get(GRB_DoubleAttr_X) > 0.5) {
                            retval.edges[l].emplace_back(e);
                        }
                    }
                }

                return retval;
            }();
            sol.embeddings.emplace_back(emb);
        }
    }
    return sol;
}

ExtendedFormulation::ExtendedFormulation(const Instance& _inst, GRBEnv& _env)
  : m_model(_env)
  , m_columns(_inst.getNbServices())
  , m_embeddingConstraints([&] {
      std::vector<GRBConstr> retval(_inst.getNbServices());
      for (const auto& [k, service] : enumerate(_inst.getServices())) {
          retval[k] = m_model.addConstr(GRBLinExpr{} <= static_cast<double>(service.nbRequested),
            fmt::format("embedding(k~{})", k));
      }
      return retval;
  }())
  , m_linkCapacityConstraints([&] {
      std::vector<GRBConstr> retval(num_edges(_inst.getPhysicalNetwork()));
      for (const auto& [e, linkCapa] : enumerate(_inst.getLinkCapacities())) {
          retval[e] =
            m_model.addConstr(GRBLinExpr{} <= linkCapa, fmt::format("linkCapacity(e~{})", e));
      }
      return retval;
  }())
  , m_cpuCapacityConstraints([&] {
      std::vector<GRBConstr> retval(num_vertices(_inst.getPhysicalNetwork()));
      for (const auto& [v, cpuCapa] : enumerate(_inst.getCPUCapacities())) {
          retval[v] =
            m_model.addConstr(GRBLinExpr{} <= cpuCapa, fmt::format("cpuConstraints(v~{})", v));
      }
      return retval;
  }())
  , m_storageCapacityConstraints([&] {
      std::vector<GRBConstr> retval(num_vertices(_inst.getPhysicalNetwork()));
      for (const auto& [v, storageCapa] : enumerate(_inst.getStorageCapacities())) {
          retval[v] = m_model.addConstr(
            GRBLinExpr{} <= storageCapa, fmt::format("storageConstraints(v~{})", v));
      }
      return retval;
  }()) {
    m_model.set(GRB_IntAttr_ModelSense, GRB_MAXIMIZE);
}

void ExtendedFormulation::addColumn(const Embedding& _emb, const Instance& _inst) {
    const auto& service = _inst.getServices()[_emb.serviceId];
    VNE_ASSERT_EQUAL(_emb.vertices.size(), num_vertices(service.network), std::equal_to<>());
    VNE_ASSERT_EQUAL(_emb.edges.size(), num_edges(service.network), std::equal_to<>());
    // Check for duplicate columns
    VNE_ASSERT(std::find_if(m_columns[_emb.serviceId].begin(),
                 m_columns[_emb.serviceId].end(),
                 [&](const auto& _col) { return _col.embedding == _emb; })
               == m_columns[_emb.serviceId].end());

    // Compute capacity used overall as physical links can be used by different virtual links
    std::map<std::size_t, double> capaUsed;
    for (const auto& [l, bwReq] : enumerate(service.getBandwidthRequirements())) {
        for (const auto& e : _emb.edges[l]) {
            capaUsed[e] += bwReq;
        }
    }
    // Now, we build the column
    GRBColumn col;
    for (const auto& [e, capa] : capaUsed) {
        col.addTerm(capa, m_linkCapacityConstraints[e]);
    }
    for (std::size_t f = 0; f < num_vertices(service.network); ++f) {
        const auto v = _emb.vertices[f];
        col.addTerm(service.getCpuRequirements()[f], m_cpuCapacityConstraints[v]);
        col.addTerm(service.getStorageRequirements()[f], m_storageCapacityConstraints[v]);
    }
    col.addTerm(1.0, m_embeddingConstraints[_emb.serviceId]);
    const auto varName =
      fmt::format("embedding(k~{})(id~{})", _emb.serviceId, m_columns[_emb.serviceId].size());
    const auto var = m_model.addVar(0.0, GRB_INFINITY, 1.0, GRB_CONTINUOUS, col, varName);
    m_columns[_emb.serviceId].emplace_back(Column{ var, _emb });
}

ExtendedFormulation::DualValues ExtendedFormulation::getDualValues() const {
    DualValues retval;
    retval.embeddingDuals.resize(m_embeddingConstraints.size());
    for (std::size_t k = 0; k < m_embeddingConstraints.size(); ++k) {
        retval.embeddingDuals[k] = m_embeddingConstraints[k].get(GRB_DoubleAttr_Pi);
        VNE_ASSERT(epsilon_greater_equal<>()(retval.embeddingDuals[k], 0.0));
    }
    retval.linkCapacityDuals.resize(m_linkCapacityConstraints.size());
    for (std::size_t e = 0; e < m_linkCapacityConstraints.size(); ++e) {
        retval.linkCapacityDuals[e] = m_linkCapacityConstraints[e].get(GRB_DoubleAttr_Pi);
        VNE_ASSERT(epsilon_greater_equal<>()(retval.linkCapacityDuals[e], 0.0));
    }
    retval.cpuCapacityDuals.resize(m_cpuCapacityConstraints.size());
    for (std::size_t v = 0; v < m_cpuCapacityConstraints.size(); ++v) {
        retval.cpuCapacityDuals[v] = m_cpuCapacityConstraints[v].get(GRB_DoubleAttr_Pi);
        VNE_ASSERT(epsilon_greater_equal<>()(retval.cpuCapacityDuals[v], 0.0));
    }
    retval.storageCapacityDuals.resize(m_storageCapacityConstraints.size());
    for (std::size_t v = 0; v < m_storageCapacityConstraints.size(); ++v) {
        retval.storageCapacityDuals[v] = m_storageCapacityConstraints[v].get(GRB_DoubleAttr_Pi);
        VNE_ASSERT(epsilon_greater_equal<>()(retval.storageCapacityDuals[v], 0.0));
    }
    return retval;
}

void ExtendedFormulation::setColumnType(char _type) {
    for (auto& cols : m_columns) {
        for (auto& col : cols) {
            col.var.set(GRB_CharAttr_VType, _type);
        }
    }
}

std::optional<Solution> ExtendedFormulation::buildSolution(const Instance& _inst) const {
    if (m_columns[0][0].var.get(GRB_CharAttr_VType) == GRB_CONTINUOUS
        || getModel().get(GRB_IntAttr_Status) == GRB_INFEASIBLE) {
        return std::nullopt;
    }
    Solution sol;
    for (std::size_t i = 0; i < _inst.getServices().size(); ++i) {
        for (const auto& col : m_columns[i]) {
            auto val = col.var.get(GRB_DoubleAttr_X);
            while (epsilon_less<>()(0.0, val)) {
                sol.embeddings.emplace_back(col.embedding);
                val -= 1.0;
            }
        }
    }
    return sol;
}


EmbeddingILP::EmbeddingILP(const Instance& _inst, Service _service, GRBEnv& _env)
  : m_physicalNetwork(_inst.getPhysicalNetwork())
  , m_service(std::move(_service))
  , m_model(std::make_unique<GRBModel>(_env))
  , m_flowVars([&] {
      std::vector<std::vector<GRBVar>> retval(
        num_edges(_inst.getPhysicalNetwork()), std::vector<GRBVar>());
      for (std::size_t e = 0; e < num_edges(_inst.getPhysicalNetwork()); ++e) {
          retval[e].resize(num_edges(m_service.network));
          for (std::size_t l = 0; l < num_edges(m_service.network); ++l) {
              retval[e][l] = getModel().addVar(
                0.0, GRB_INFINITY, 0.0, GRB_BINARY, fmt::format("flow(e~{})(l~{})", e, l));
          }
      }
      return retval;
  }())
  , m_vnfVars([&] {
      std::vector<std::vector<GRBVar>> retval(num_vertices(m_service.network),
        std::vector<GRBVar>(num_vertices(_inst.getPhysicalNetwork())));
      for (std::size_t f = 0; f < num_vertices(m_service.network); ++f) {
          if (m_service.fixedLocations[f] == Service::NoFixedLocation) {
              for (std::size_t v = 0; v < num_vertices(_inst.getPhysicalNetwork()); ++v) {
                  retval[f][v] = getModel().addVar(
                    0.0, GRB_INFINITY, 0.0, GRB_BINARY, fmt::format("vnf(v~{})(f~{})", v, f));
              }
          } else {
              for (std::size_t v = 0; v < num_vertices(_inst.getPhysicalNetwork()); ++v) {
                  retval[f][v] = getModel().addVar(
                    0.0, 0.0, 0.0, GRB_BINARY, fmt::format("vnf(v~{})(f~{})", v, f));
              }
              for (const auto v : m_service.fixedLocations[f]) {
                  retval[f][v].set(GRB_DoubleAttr_UB, 1.0);
              }
          }
      }
      return retval;
  }())
  , m_vnfToGraphConstraints([&] {
      std::vector<GRBConstr> retval(num_vertices(m_service.network));
      for (std::size_t f = 0; f < num_vertices(m_service.network); ++f) {
          GRBLinExpr expr;
          for (std::size_t v = 0; v < num_vertices(_inst.getPhysicalNetwork()); ++v) {
              expr += m_vnfVars[f][v];
          }
          retval[f] = getModel().addConstr(expr == 1, fmt::format("vnfToGraph(f~{})", f));
      }
      return retval;
  }())
  , m_oneFunctionPerNodeConstraints([&] {
      std::vector<GRBConstr> retval(num_vertices(_inst.getPhysicalNetwork()), GRBConstr());
      for (std::size_t v = 0; v < num_vertices(_inst.getPhysicalNetwork()); ++v) {
          GRBLinExpr expr;
          for (std::size_t f = 0; f < num_vertices(m_service.network); ++f) {
              expr += m_vnfVars[f][v];
          }
          retval[v] = getModel().addConstr(expr <= 1, fmt::format("oneFunctionPerNode(v~{})", v));
      }
      return retval;
  }())
  , m_flowConservationConstraints([&] {
      std::vector<std::vector<GRBConstr>> retval(num_vertices(_inst.getPhysicalNetwork()),
        std::vector<GRBConstr>(num_edges(m_service.network)));
      for (std::size_t v = 0; v < num_vertices(_inst.getPhysicalNetwork()); ++v) {
          for (const auto& ved : boost::make_iterator_range(edges(m_service.network))) {
              const auto l = m_service.network[ved].id;
              GRBLinExpr expr;
              for (const auto& ped :
                boost::make_iterator_range(out_edges(v, _inst.getPhysicalNetwork()))) {
                  const auto e = _inst.getPhysicalNetwork()[ped].id;
                  expr += m_flowVars[e][l];
              }
              for (const auto& ped :
                boost::make_iterator_range(in_edges(v, _inst.getPhysicalNetwork()))) {
                  const auto e = _inst.getPhysicalNetwork()[ped].id;
                  expr -= m_flowVars[e][l];
              }
              const auto& [f1, f2] = incident(ved, m_service.network);
              expr += -m_vnfVars[f1][v] + m_vnfVars[f2][v];
              retval[v][l] =
                getModel().addConstr(expr == 0, fmt::format("flowConservation(v~{})(l~{})", v, l));
          }
      }
      return retval;
  }())
  , m_serviceDelayConstraints([&] {
      GRBLinExpr expr;
      for (std::size_t e = 0; e < num_edges(_inst.getPhysicalNetwork()); ++e) {
          for (std::size_t l = 0; l < num_edges(m_service.network); ++l) {
              expr += _inst.getLinkDelays()[e] * m_flowVars[e][l];
          }
      }
      return getModel().addConstr(expr <= GRB_INFINITY, "serviceDelay");
  }())
  , m_vnfDelayConstraints([&] {
      std::vector<GRBConstr> retval(num_edges(m_service.network));
      for (std::size_t l = 0; l < num_edges(m_service.network); ++l) {
          GRBLinExpr expr;
          for (std::size_t e = 0; e < num_edges(_inst.getPhysicalNetwork()); ++e) {
              expr += _inst.getLinkDelays()[e] * m_flowVars[e][l];
          }
          retval[l] = getModel().addConstr(
            expr <= m_service.getDelayRequirements()[l], fmt::format("vnfDelay(l~{})", l));
      }
      return retval;
  }())

{
    getModel().set(GRB_IntAttr_ModelSense, GRB_MINIMIZE);
}

void EmbeddingILP::setServiceDelay(double _val) {
    m_serviceDelayConstraints.set(GRB_DoubleAttr_RHS, _val);
}


void EmbeddingILP::setObjectiveCoefficient(const std::vector<double>& _flowCoeff,
  const std::vector<double>& _cpuCoeff,
  const std::vector<double>& _storageCoeff) {

    for (std::size_t e = 0; e < _flowCoeff.size(); ++e) {
        for (std::size_t l = 0; l < num_edges(m_service.network); ++l) {
            m_flowVars[e][l].set(
              GRB_DoubleAttr_Obj, m_service.getBandwidthRequirements()[l] * _flowCoeff[e]);
        }
    }

    for (std::size_t v = 0; v < _cpuCoeff.size(); ++v) {
        for (std::size_t f = 0; f < num_vertices(m_service.network); ++f) {
            m_vnfVars[f][v].set(GRB_DoubleAttr_Obj,
              m_service.getCpuRequirements()[f] * _cpuCoeff[v]
                + m_service.getStorageRequirements()[f] * _storageCoeff[v]);
        }
    }
}

std::vector<Embedding> EmbeddingILP::getEmbeddings(std::size_t _serviceId) {
    std::vector<Embedding> retval;
    for (int i = 0; i < getModel().get(GRB_IntAttr_SolCount); ++i) {
        getModel().set(GRB_IntParam_SolutionNumber, i);
        Embedding emb;
        emb.serviceId = _serviceId;
        emb.edges.resize(num_edges(m_service.network));
        for (std::size_t l = 0; l < num_edges(m_service.network); ++l) {
            for (const auto& ped : boost::make_iterator_range(edges(m_physicalNetwork))) {
                const auto e = m_physicalNetwork[ped].id;
                if (m_flowVars[e][l].get(GRB_DoubleAttr_Xn) > 0.5) {
                    emb.edges[l].emplace_back(e);
                }
            }
        }
        emb.vertices.resize(num_vertices(m_service.network));
        for (std::size_t f = 0; f < num_vertices(m_service.network); ++f) {
            for (std::size_t v = 0; v < num_vertices(m_physicalNetwork); ++v) {
                if (m_vnfVars[f][v].get(GRB_DoubleAttr_Xn) > 0.5) {
                    emb.vertices[f] = v;
                    break;
                }
            }
        }
        retval.emplace_back(emb);
    }
    return retval;
}
Embedding EmbeddingILP::getEmbedding(std::size_t _serviceId) const {
    Embedding retval;
    retval.serviceId = _serviceId;
    retval.edges.resize(num_edges(m_service.network));
    for (std::size_t l = 0; l < num_edges(m_service.network); ++l) {
        for (const auto& ped : boost::make_iterator_range(edges(m_physicalNetwork))) {
            const auto e = m_physicalNetwork[ped].id;
            if (m_flowVars[e][l].get(GRB_DoubleAttr_X) > 0.5) {
                retval.edges[l].emplace_back(e);
            }
        }
    }
    retval.vertices.resize(num_vertices(m_service.network));
    for (std::size_t f = 0; f < num_vertices(m_service.network); ++f) {
        for (std::size_t v = 0; v < num_vertices(m_physicalNetwork); ++v) {
            if (m_vnfVars[f][v].get(GRB_DoubleAttr_X) > 0.5) {
                retval.vertices[f] = v;
                break;
            }
        }
    }
    return retval;
}

double getReducedCost(const ExtendedFormulation::DualValues& _duals,
  const Embedding& _col,
  const Instance& _inst) {
    const auto& service = _inst.getServices()[_col.serviceId];

    std::map<std::size_t, double> capaUsed;
    for (std::size_t l = 0; l < num_edges(service.network); ++l) {
        for (const auto& e : _col.edges[l]) {
            capaUsed[e] += service.getBandwidthRequirements()[l];
        }
    }

    const auto comb = [&](PhysicalNetwork::vertex_descriptor _vd,
                        VirtualNetwork::vertex_descriptor _fd) {
        const auto& f = service.network[_fd].id;
        return service.getCpuRequirements()[f] * _duals.cpuCapacityDuals[_vd]
               + service.getStorageRequirements()[f] * _duals.storageCapacityDuals[_vd];
    };
    const auto nodeRC = std::inner_product(_col.vertices.begin(),
      _col.vertices.end(),
      vertices(service.network).first,
      0.0,
      std::plus<double>(),
      comb);
    return 1 - _duals.embeddingDuals[_col.serviceId] - nodeRC
           - std::accumulate(
             capaUsed.begin(), capaUsed.end(), 0.0, [&](auto _acc, const auto _kval) {
                 _acc += _kval.second * _duals.linkCapacityDuals[_kval.first];
                 return _acc;
             });
}


}// namespace VNE

