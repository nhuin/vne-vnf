/**
 * \file src/algorithm.hpp
 * This file contains all definitions for the algorithms used in solving the VNE-VNF problem
 */
#ifndef ALGORITHM_HPP
#define ALGORITHM_HPP

#include "vne.hpp"
#include "models.hpp"
#include <random>

class GRBEnv;

namespace VNE {

template<typename MasterFunction, typename PricingFunction>
concept GenerateDuals = requires(MasterFunction _mf, PricingFunction _pf) {
    { _pf(*_mf({})) };
};

template<typename MasterFunction, typename PricingFunction>
concept GenerateColumns = requires(PricingFunction _pf, MasterFunction _mf) {
    { _mf(*_pf(*_mf({}))) };
};

template<typename MasterFunction, typename PricingFunction>
requires GenerateDuals<MasterFunction, PricingFunction>
void columnGeneration(MasterFunction _master, PricingFunction _pricing) {
    auto duals = _master({});
    if (!duals.has_value()) {
        return;
    }
    auto cols = _pricing(*duals);
    while (cols.has_value()) {
        duals = _master(*cols);
        if (!duals.has_value()) {
            return;
        }
        cols = _pricing(*duals);
    }
}
/**
 * Run the column generation algorithm
 **/
ExtendedFormulation columnGeneration(const Instance& _inst, GRBEnv& _env);

std::pair<std::optional<Solution>, double> cgAndBranchAndBound(const Instance& _inst, GRBEnv& _env);

std::pair<std::optional<Solution>, double>
  cgAndRandomizedRounding(const Instance& _inst, GRBEnv& _env, std::mt19937& _gen);
}// namespace VNE
#endif
