#ifndef VNE_HPP
#define VNE_HPP

#include <boost/graph/adjacency_list.hpp>
#include <nlohmann/json_fwd.hpp>
#include <spdlog/spdlog.h>
#include <boost/range/adaptor/transformed.hpp>
#include <boost/range/adaptors.hpp>
#include <cstddef>
#include <numeric>
#include <optional>
#include <cassert>
#include <random>

#ifndef DISABLE_VNE_ASSERT
#define VNE_ASSERT(x) assert(x)
#else
#define VNE_ASSERT(x) (void)(0)
#endif

template<typename Type, typename StringType, typename EqualityFunction = std::equal_to<Type>>
constexpr void abortWhenNotEqual(Type&& _t1,
  StringType _name1,
  Type&& _t2,
  StringType _name2,
  EqualityFunction _func = EqualityFunction()) {
    if (!_func(std::forward<Type>(_t1), std::forward<Type>(_t2))) {
        spdlog::error("Equality assert failed : {} != {} ({} != {})", _name1, _name2, _t1, _t2);
        abort();
    }
}
#ifndef DISABLE_VNE_ASSERT
#define VNE_ASSERT_EQUAL(x, y, f) abortWhenNotEqual(x, #x, y, #y, f)

#else
#define VNE_ASSERT_EQUAL(x, y) (void)(0)
#endif


template<typename ContainerType>
auto enumerate(ContainerType&& _container, std::size_t _index = 0ul) {
    return boost::adaptors::index(
             std::forward<ContainerType>(_container), static_cast<long>(_index))
           | boost::adaptors::transformed([](auto _it) {
                 return std::make_pair(static_cast<std::size_t>(_it.index()), _it.value());
             });
}
/**
 * The VNE namespace contains all algorithms and data structure for solving the VNE problem
 */
namespace VNE {

/**
 * PhysicalNode contains the properties of a physical node (cpu and memory)
 */
struct PhysicalNode
{
    std::size_t id{ std::numeric_limits<std::size_t>::max() };
};


/**
 * PhysicalLink contains the properties of a physical link (capacity and delay)
 */
struct PhysicalLink
{
    std::size_t id{ std::numeric_limits<std::size_t>::max() };
};

/**
 * Alias for a boost adjacency_list graph that represents a physical network
 */
using PhysicalNetwork = boost::
  adjacency_list<boost::vecS, boost::vecS, boost::bidirectionalS, PhysicalNode, PhysicalLink>;

/**
 * VirtualNode contains the constraints of the virtual node (cpu and memory usage)
 */
struct VirtualNode
{
    std::size_t id{ std::numeric_limits<std::size_t>::max() };
};


/**
 * VirtualLink contains the constraints of the virtual link (capacity and delay)
 */
struct VirtualLink
{
    std::size_t id{ std::numeric_limits<std::size_t>::max() };
};

/**
 * Alias for a boost adjacency_list graph that represents a VNF graph
 */
using VirtualNetwork =
  boost::adjacency_list<boost::vecS, boost::vecS, boost::bidirectionalS, VirtualNode, VirtualLink>;

struct Service
{
    VirtualNetwork network;
    inline const static std::vector<PhysicalNetwork::vertex_descriptor>
      NoFixedLocation{};///< Represents a function that can be placed anywhere in the
                        ///< network
    std::vector<std::vector<PhysicalNetwork::vertex_descriptor>>
      fixedLocations;///< Indicates the set of nodes where a function can be placed.
    std::vector<double> cpuRequirements;
    std::vector<double> storageRequirements;
    std::vector<double> bandwidthRequirements;
    std::vector<double> delayRequirements;
    double serviceDelay;///< Bound on the sum of all virtual links delay
    std::size_t nbRequested;///< Number of times this service graph is requested
    const auto& getCpuRequirements() const { return cpuRequirements; }
    const auto& getStorageRequirements() const { return storageRequirements; }
    const auto& getBandwidthRequirements() const { return bandwidthRequirements; }
    const auto& getDelayRequirements() const { return delayRequirements; }
};

/**
 * Instance represents an instance of the VNE problem
 */
class Instance {
    PhysicalNetwork network;
    std::vector<double> cpuCapacity;
    std::vector<double> storageCapacity;
    std::vector<double> linkDelays;
    std::vector<double> linkCapacity;
    std::vector<Service> services;

  public:
    Instance(PhysicalNetwork _network,
      std::vector<double> _cpuCapacity,
      std::vector<double> _storageCapacity,
      std::vector<double> _linkDelays,
      std::vector<double> _linkCapacity,
      std::vector<Service> _services);
    auto getNbServices() const { return services.size(); }
    const auto& getServices() const { return services; }
    const auto& getPhysicalNetwork() const { return network; }
    const auto& getCPUCapacities() const { return cpuCapacity; }
    const auto& getStorageCapacities() const { return cpuCapacity; }
    const auto& getLinkDelays() const { return linkDelays; }
    const auto& getLinkCapacities() const { return linkCapacity; }

    auto getTotalNbRequest() const {
        return std::accumulate(
          services.begin(), services.end(), 0.0, [&](std::size_t _acc, const Service& _s) {
              return _acc + _s.nbRequested;
          });
    }
};


template<typename VNIteratorType>
std::map<Service, std::size_t> getSimilarServices(VNIteratorType _first, VNIteratorType _last) {
    std::map<Service, std::size_t> retval;
    for (; _first != _last; ++_first) {
        retval[*_first]++;
    }
    return retval;
}

bool operator<(const Service& _vn1, const Service& _vn2);

struct Embedding
{
    std::size_t serviceId;
    std::vector<PhysicalNetwork::vertex_descriptor> vertices;
    std::vector<std::vector<std::size_t>> edges;

    friend bool operator==(const Embedding& _emb1, const Embedding& _emb2);
};

double getEndToEndLatency(const Instance& _inst, const Embedding& _emb);

template<typename Graph, typename EdgeRange>
auto edge_properties(const Graph& _g, EdgeRange&& _rng) {
    return boost::make_iterator_range(_rng)
           | boost::adaptors::transformed([&](const auto& _vd) { return _g[_vd]; });
}

template<typename Graph> auto edge_properties(const Graph& _g) {
    return edge_properties(_g, edges(_g));
    /*
    return boost::make_iterator_range(edges(_g))
           | boost::adaptors::transformed([&](const auto& _vd) { return _g[_vd]; });
   */
}


template<typename Graph> auto vertex_properties(const Graph& _g) {
    return boost::make_iterator_range(vertices(_g))
           | boost::adaptors::transformed([&](const auto& _vd) { return _g[_vd]; });
}

struct Solution
{
    std::vector<std::optional<Embedding>> embeddings;
};

/**
 * Given an embedding, return true if its respect the delay constraints and the flow conservation
 * constraints
 */
bool isValid(const Embedding& _emb, const Instance& _inst);

VirtualNetwork createRing(std::size_t _size);

VirtualNetwork createChain(std::size_t _size);

VirtualNetwork createStar(std::size_t _size);

Service loadService(const nlohmann::json& _service);
Instance loadInstance(const nlohmann::json& _topologyJSON) ;
std::vector<Service> loadServices(const std::string& _filename);
VNE::PhysicalNetwork loadNetworkFromFile(const std::string& _filename);

}// namespace VNE

#endif
