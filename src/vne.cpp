#include "vne.hpp"
#include <boost/graph/graphml.hpp>
#include <boost/property_map/dynamic_property_map.hpp>
#include <boost/range/algorithm/mismatch.hpp>
#include <boost/range/irange.hpp>
#include <boost/range/adaptors.hpp>
#include <boost/range/iterator_range_core.hpp>
#include <iterator>
#include <nlohmann/json.hpp>
#include <algorithm>
#include <fstream>
#include <nlohmann/json_fwd.hpp>
#include <spdlog/spdlog.h>
#include <fmt/format.h>
#include <utility>

namespace VNE {

Instance::Instance(PhysicalNetwork _network,
  std::vector<double> _cpuCapacity,
  std::vector<double> _storageCapacity,
  std::vector<double> _linkDelays,
  std::vector<double> _linkCapacity,
  std::vector<Service> _services)
  : network(_network)
  , cpuCapacity(_cpuCapacity)
  , storageCapacity(_storageCapacity)
  , linkDelays(_linkDelays)
  , linkCapacity(_linkCapacity)
  , services(_services)

{}

bool operator==(const Embedding& _emb1, const Embedding& _emb2) {
    return _emb1.serviceId == _emb2.serviceId && _emb1.vertices == _emb2.vertices
           && _emb1.edges == _emb2.edges;
}

double getEndToEndLatency(const Instance& _inst, const Embedding& _emb) {
    double retval = 0.0;
    for (const auto& edges : _emb.edges) {
        for (const auto e : edges) {
            retval += _inst.getLinkDelays()[e];
        }
    }
    return retval;
}


VirtualNetwork createRing(std::size_t _size) {
    std::vector<std::pair<std::size_t, std::size_t>> edgeList;
    for (std::size_t l = 0; l < _size - 1; ++l) {
        edgeList.emplace_back(l, l + 1);
        edgeList.emplace_back(l + 1, l);
    }
    // Create link to achieve ring
    edgeList.emplace_back(_size - 1, 0);
    edgeList.emplace_back(0, _size - 1);
    std::vector<VirtualLink> props(edgeList.size());
    std::generate(props.begin(), props.end(), [i = 0ul]() mutable { return VirtualLink{ i++ }; });

    // Create graph and add node properties
    VNE::VirtualNetwork retval(edgeList.begin(), edgeList.end(), props.begin(), _size);
    for (std::size_t f = 0; f < num_vertices(retval); ++f) {
        retval[f].id = f;
    }
    return retval;
}

VirtualNetwork createChain(std::size_t _size) {
    std::vector<std::pair<std::size_t, std::size_t>> edgeList;
    for (std::size_t l = 0; l < _size - 1; ++l) {
        edgeList.emplace_back(l, l + 1);
        edgeList.emplace_back(l + 1, l);
    }
    std::vector<VirtualLink> props(edgeList.size());
    std::generate(props.begin(), props.end(), [i = 0ul]() mutable { return VirtualLink{ i++ }; });
    VNE::VirtualNetwork retval(edgeList.begin(), edgeList.end(), props.begin(), _size);
    for (std::size_t f = 0; f < num_vertices(retval); ++f) {
        retval[f].id = f;
    }
    return retval;
}

VirtualNetwork createStar(std::size_t _size) {
    std::vector<std::pair<std::size_t, std::size_t>> edgeList;
    for (std::size_t l = 1; l < _size - 1; ++l) {
        edgeList.emplace_back(0, l);
        edgeList.emplace_back(l, 0);
    }
    std::vector<VirtualLink> props(edgeList.size());
    std::generate(props.begin(), props.end(), [i = 0ul]() mutable { return VirtualLink{ i++ }; });
    VNE::VirtualNetwork retval(edgeList.begin(), edgeList.end(), props.begin(), _size);
    for (std::size_t f = 0; f < num_vertices(retval); ++f) {
        retval[f].id = f;
    }
    return retval;
}

std::vector<Service> loadServices(const std::string& _filename) {
    const auto services = nlohmann::json::parse(std::ifstream(_filename));
    std::vector<Service> retval;
    for (const auto& json_service : services) {
        Service service;
        const auto edgeList = json_service["edgeList"]
                              | boost::adaptors::transformed(
                                [](const auto& edge) { return std::make_pair(edge[0], edge[1]); });
        const auto props =
          boost::irange(json_service.size())
          | boost::adaptors::transformed([](const auto i) { return VirtualLink{ i }; });

        service.network =
          VirtualNetwork(edgeList.begin(), edgeList.end(), props.begin(), json_service["order"]);
        for (const auto& [i, vd] :
          enumerate(boost::make_iterator_range(vertices(service.network)))) {
            service.network[vd].id = i;
        }
        service.cpuRequirements = json_service["cpuRequirements"].get<std::vector<double>>();
        service.storageRequirements =
          json_service["storageRequirements"].get<std::vector<double>>();
        service.bandwidthRequirements =
          json_service["bandwidthRequirements"].get<std::vector<double>>();
        service.delayRequirements = json_service["delayRequirements"].get<std::vector<double>>();
        service.serviceDelay = json_service["serviceDelay"];
        service.nbRequested = json_service["nbRequested"];
        service.fixedLocations = json_service["fixedLocations"];
        retval.emplace_back(std::move(service));
    }
    return retval;
}

Service loadService(const nlohmann::json& _service) {
        Service service;
        const auto edgeList = _service["edgeList"]
                              | boost::adaptors::transformed(
                                [](const auto& edge) { return std::make_pair(edge[0], edge[1]); });
        const auto props =
          boost::irange(_service.size())
          | boost::adaptors::transformed([](const auto i) { return VirtualLink{ i }; });

        service.network =
            VirtualNetwork(edgeList.begin(), edgeList.end(), props.begin(), _service["order"]);
        for (const auto& [i, vd] :
                enumerate(boost::make_iterator_range(vertices(service.network)))) {
            service.network[vd].id = i;
        }
        service.cpuRequirements = _service["cpuRequirements"].get<std::vector<double>>();
        service.storageRequirements =
            _service["storageRequirements"].get<std::vector<double>>();
        service.bandwidthRequirements =
            _service["bandwidthRequirements"].get<std::vector<double>>();
        service.delayRequirements = _service["delayRequirements"].get<std::vector<double>>();
        service.serviceDelay = _service["serviceDelay"];
        service.nbRequested = _service["nbRequested"];
        service.fixedLocations = _service["fixedLocations"];
        return service;
}

Instance loadInstance(const nlohmann::json&     _instanceJson) {
    std::vector<std::pair<std::size_t, std::size_t>> edgeList;
    std::vector<VNE::PhysicalLink> edgeProps;
    std::vector<double> linkDelays;
    std::vector<double> linkCapacity;
    for (const auto edge : _instanceJson["network"]["edges"]) {
        edgeList.emplace_back(edge["src"], edge["dst"]);
        edgeProps.emplace_back(VNE::PhysicalLink{ edgeProps.size() });
        linkDelays.emplace_back(edge["delay"]);
        linkCapacity.emplace_back(edge["capacity"]);
    }
    std::vector<double> cpuCapacity;
    std::vector<double> storageCapacity;
    for (const auto node : _instanceJson["network"]["nodes"]) {
        cpuCapacity.emplace_back(node["cpu"]);
        storageCapacity.emplace_back(node["storage"]);
    }

    VNE::PhysicalNetwork network(
      edgeList.begin(), edgeList.end(), edgeProps.begin(), storageCapacity.size());
    for (const auto v : boost::make_iterator_range(vertices(network))) {
        network[v].id = v;
    }
    std::vector<Service> services;
    for (const auto service : _instanceJson["services"]) {
        services.emplace_back(loadService(service));
    }
    return Instance(network, cpuCapacity, storageCapacity, linkDelays, linkCapacity, services);
}

VNE::PhysicalNetwork loadNetworkFromFile(const std::string& _filename) {
    std::ifstream ifs(_filename);
    boost::adjacency_list<boost::vecS, boost::vecS, boost::undirectedS> graph;
    boost::dynamic_properties dp(boost::ignore_other_properties);
    read_graphml(ifs, graph, dp);
    /*
    VNE::PhysicalNetwork graph;

    boost::dynamic_properties dynProperties(boost::ignore_other_properties);
    // dynProperties.property("id", get(&VNE::PhysicalNode::id, graph));
    // dynProperties.property("id", get(&VNE::PhysicalLink::id, graph));
    read_graphml(ifs, graph, dynProperties);
    std::size_t e = 0;
    for (const auto ed : boost::make_iterator_range(edges(graph))) {
        graph[ed].id = e++;
    }
    */

    std::vector<std::pair<std::size_t, std::size_t>> edgeList;
    std::vector<VNE::PhysicalLink> edgeProps;
    for (const auto ed : boost::make_iterator_range(edges(graph))) {
        edgeList.emplace_back(source(ed, graph), target(ed, graph));
        edgeList.emplace_back(target(ed, graph), source(ed, graph));
        edgeProps.emplace_back(VNE::PhysicalLink{ edgeProps.size() });
        edgeProps.emplace_back(VNE::PhysicalLink{ edgeProps.size() });
    }
    VNE::PhysicalNetwork retval(
      edgeList.begin(), edgeList.end(), edgeProps.begin(), num_vertices(graph));
    for (const auto v : boost::make_iterator_range(vertices(retval))) {
        retval[v].id = v;
    }
    return retval;
}

}// namespace VNE
