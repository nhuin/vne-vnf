#include "vne.hpp"
#include <boost/range/adaptor/transformed.hpp>
#include <boost/graph/graph_utility.hpp>
#include <boost/range/iterator_range_core.hpp>
#include <limits>
#include <optional>
#include <pybind11/pybind11.h>
#include <random>
#include <ranges>
#include <fmt/printf.h>
#include <pybind11/stl.h>
#include <spdlog/spdlog.h>

#define STRINGIFY(x) #x
#define MACRO_STRINGIFY(x) STRINGIFY(x)

#include "algorithm.hpp"


namespace pybind11 { namespace detail {
    template<typename T>
    struct type_caster<boost::optional<T>> : optional_caster<boost::optional<T>>
    {
    };
}}// namespace pybind11::detail

PYBIND11_MODULE(_core, mod) {
    pybind11::class_<VNE::VirtualNetwork>(mod, "VirtualNetwork")
      .def(pybind11::init<>([](const std::vector<std::pair<std::size_t, std::size_t>>& _edgeList,
                              std::size_t _nbVertices) {
          auto g = VNE::VirtualNetwork{ _edgeList.begin(), _edgeList.end(), _nbVertices };
          std::size_t id = 0;
          for (const auto ed : boost::make_iterator_range(edges(g))) {
              g[ed].id = id++;
          }
          for (std::size_t v = 0; v < num_vertices(g); ++v) {
              g[v].id = v;
          }
          return g;
      }),
        pybind11::arg("edge_list"),
        pybind11::arg("nb_vertices"))
      .def("__repr__",
        [](const VNE::VirtualNetwork& _network) {
            return fmt::format("<vne_vnf.VirtualNetwork with {} nodes and {} links>",
              num_vertices(_network),
              num_edges(_network));
        })
      .def("getEdges",
        [](
          const VNE::VirtualNetwork& _network) -> std::vector<std::pair<std::size_t, std::size_t>> {
            const auto range =
              boost::make_iterator_range(edges(_network))
              | boost::adaptors::transformed([&](const auto ed) { return incident(ed, _network); });
            return { range.begin(), range.end() };
        })
      .def(
        "order",
        [](VNE::VirtualNetwork* _this) { return num_vertices(*_this); },
        "Returns the number of vertices in the network")
      .def(
        "size",
        [](VNE::VirtualNetwork* _this) { return num_edges(*_this); },
        "Returns the number of edges in the network");

    pybind11::class_<VNE::Service>(mod, "Service")
      .def(pybind11::init<VNE::VirtualNetwork,
             std::vector<std::vector<VNE::PhysicalNetwork::vertex_descriptor>>,
             std::vector<double>,
             std::vector<double>,
             std::vector<double>,
             std::vector<double>,
             double,
             std::size_t>(),
        "Build a service",
        pybind11::arg("network"),
        pybind11::arg("fixedLocations"),
        pybind11::arg("CPU_requirements"),
        pybind11::arg("storage_requirements"),
        pybind11::arg("bandwidth_requirements"),
        pybind11::arg("delay_requirements"),
        pybind11::arg("service_delay"),
        pybind11::arg("nb_requested"))
      .def_readwrite("network", &VNE::Service::network)
      .def_readwrite("CPU_requirements", &VNE::Service::cpuRequirements)
      .def_readwrite("storage_requirements", &VNE::Service::storageRequirements)
      .def_readwrite("bandwidth_requirements", &VNE::Service::bandwidthRequirements)
      .def_readwrite("delay_requirements", &VNE::Service::delayRequirements)
      .def_readwrite("service_delay", &VNE::Service::serviceDelay)
      .def_readwrite("nb_requested", &VNE::Service::nbRequested)
      .def("__repr__", [](const VNE::Service& _service) {
          return fmt::format("<vne_vnf.Service with {} functions and {} virtual links>",
            num_vertices(_service.network),
            num_edges(_service.network));
      });

    pybind11::class_<VNE::Instance>(mod, "Instance")
      .def(pybind11::init<VNE::PhysicalNetwork,
             std::vector<double>,
             std::vector<double>,
             std::vector<double>,
             std::vector<double>,
             std::vector<VNE::Service>>(),
        pybind11::arg("network"),
        pybind11::arg("CPU_capacities"),
        pybind11::arg("storage_capacities"),
        pybind11::arg("delays"),
        pybind11::arg("bandwidths"),
        pybind11::arg("services"));

    pybind11::class_<VNE::PhysicalNetwork>(mod, "PhysicalNetwork")
      .def(pybind11::init<>([](const std::vector<std::pair<std::size_t, std::size_t>>& _edgeList,
                              std::size_t _nbVertices) {
          auto g = VNE::PhysicalNetwork{ _edgeList.begin(), _edgeList.end(), _nbVertices };
          std::size_t id = 0;
          for (const auto ed : boost::make_iterator_range(edges(g))) {
              g[ed].id = id++;
          }
          for (std::size_t v = 0; v < num_vertices(g); ++v) {
              g[v].id = v;
          }
          return g;
      }))
      .def("__repr__",
        [](const VNE::PhysicalNetwork& _network) {
            return fmt::format("<vne_vnf.PhysicalNetwork with {} nodes and {} links>",
              num_vertices(_network),
              num_edges(_network));
        })
      .def("getEdges",
        [](const VNE::PhysicalNetwork& _network)
          -> std::vector<std::pair<std::size_t, std::size_t>> {
            const auto range =
              boost::make_iterator_range(edges(_network))
              | boost::adaptors::transformed([&](const auto ed) { return incident(ed, _network); });
            return { range.begin(), range.end() };
        })
      .def(
        "order",
        [](VNE::PhysicalNetwork* _this) { return num_vertices(*_this); },
        "Returns the number of vertices in the network")
      .def(
        "size",
        [](VNE::PhysicalNetwork* _this) { return num_edges(*_this); },
        "Returns the number of edges in the network");
    // Function
    mod.def("createRing", &VNE::createRing, "Create a virtual network as a ring");
    mod.def("createStar", &VNE::createStar, "Create a virtual network as a star");
    mod.def("createChain", &VNE::createChain, "Create a virtual network as a chain");

    pybind11::class_<VNE::Embedding>(mod, "Embedding")
      .def_readwrite("serviceId", &VNE::Embedding::serviceId)
      .def_readwrite("vertices", &VNE::Embedding::vertices)
      .def_readwrite("edges", &VNE::Embedding::edges);

    pybind11::class_<VNE::Solution>(mod, "Solution")
      .def_readwrite("embeddings", &VNE::Solution::embeddings);

    pybind11::class_<GRBEnv>(mod, "GRBEnv").def(pybind11::init<>());

    mod.def(
      "cgAndRandomizedRounding",
      [](const VNE::Instance& _inst,
        GRBEnv& _env,
        std::size_t _seed) -> std::pair<std::optional<VNE::Solution>, double> {
          try {
              auto seed = std::seed_seq({ _seed });
              std::mt19937 gen(seed);
              return VNE::cgAndRandomizedRounding(_inst, _env, gen);
          } catch (GRBException e) {
              return { std::nullopt, -std::numeric_limits<double>::infinity() };
              fmt::print("GUROBI error {}: {}", e.getErrorCode(), e.getMessage());
          }
      },
      "Solve the problem with column generation followed by randomized-rounding");

    mod.def(
      "cgAndBranchAndBound",
      [](const VNE::Instance& _inst,
        GRBEnv& _env) -> std::pair<std::optional<VNE::Solution>, double> {
          try {
              return VNE::cgAndBranchAndBound(_inst, _env);
          } catch (GRBException& e) {
              return { std::nullopt, -std::numeric_limits<double>::infinity() };
              fmt::print("GUROBI error {}: {}", e.getErrorCode(), e.getMessage());
          }
      },
      "Solve the problem with column generation followed by branch-and-bound");

    mod.def(
      "ilp",
      [](const VNE::Instance& _inst, GRBEnv& _env, bool _verbose, double _timeLimit)
        -> std::pair<bool, std::optional<VNE::Solution>> {
          try {
              VNE::CompactILP ilp(_inst, _env);
              ilp.getModel().set(GRB_DoubleParam_TimeLimit, _timeLimit);
              ilp.getModel().set(GRB_IntParam_OutputFlag, _verbose);
              ilp.getModel().optimize();
              return { ilp.getModel().get(GRB_IntAttr_Status) == GRB_OPTIMAL,
                  ilp.buildSolution(_inst) };
          } catch (GRBException& e) {
              return { false, std::nullopt };
              fmt::print("GUROBI error {}: {}", e.getErrorCode(), e.getMessage());
          }
      },
      "Solve the problem with the compact formulation");

#ifdef VERSION_INFO
    mod.attr("__version__") = MACRO_STRINGIFY(VERSION_INFO);
#else
    mod.attr("__version__") = "dev";
#endif
}
