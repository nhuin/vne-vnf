#ifndef UTILS_HPP
#define UTILS_HPP

#include <algorithm>
#include <cstdlib>
#include <gurobi_c++.h>

template<typename RealType = double> struct epsilon_equal
{
    RealType precision;

    explicit epsilon_equal(RealType _precision = 1e-6)
      : precision(_precision) {}

    bool operator()(RealType _lhs, RealType _rhs) const noexcept {
        return std::abs(_lhs - _rhs)
               <= precision * std::max({ 1.0, std::abs(_lhs), std::abs(_rhs) });
    }
};

template<typename RealType = double> struct epsilon_less
{
    RealType precision;

    explicit epsilon_less(RealType _precision = 1e-6)
      : precision(_precision) {}
    epsilon_less(const epsilon_less&) = default;
    epsilon_less& operator=(epsilon_less&&) = default;
    epsilon_less(epsilon_less&&) = default;
    epsilon_less& operator=(const epsilon_less&) = default;
    ~epsilon_less() = default;

    bool operator()(RealType _lhs, RealType _rhs) const noexcept { return _lhs + precision < _rhs; }
};

template<typename RealType = double> struct epsilon_greater_equal
{
    RealType precision;

    explicit epsilon_greater_equal(RealType _precision = 1e-6)
      : precision(_precision) {}
    epsilon_greater_equal(const epsilon_greater_equal&) = default;
    epsilon_greater_equal& operator=(epsilon_greater_equal&&) = default;
    epsilon_greater_equal(epsilon_greater_equal&&) = default;
    epsilon_greater_equal& operator=(const epsilon_greater_equal&) = default;
    ~epsilon_greater_equal() = default;

    bool operator()(RealType _lhs, RealType _rhs) const noexcept {
        return !epsilon_less<RealType>(precision)(_lhs, _rhs);
    }
};

#endif

