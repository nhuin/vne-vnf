#ifndef MODELS_HPP
#define MODELS_HPP

#include <algorithm>
#include <unordered_map>
#include <utility>
#include <vector>
#include "gurobi_c++.h"

#include <boost/multi_array.hpp>
#include "utils.hpp"
#include "vne.hpp"
#include <boost/range/adaptors.hpp>


namespace VNE {

/**
 * CompactILP represents the compact formulation used to solve the VNE problem
 */
class CompactILP {
    GRBModel m_model;

    // Variables
    std::vector<std::vector<std::vector<std::vector<GRBVar>>>> m_flowVars;
    std::vector<std::vector<std::vector<std::vector<GRBVar>>>> m_vnfVars;

    // Objective function

    GRBLinExpr m_obj;

    // Constraints
    std::vector<std::vector<std::vector<GRBConstr>>> m_vnfToGraphConstraints;
    std::vector<std::vector<std::vector<GRBConstr>>> m_oneFunctionPerNodeConstraints;
    std::vector<std::vector<std::vector<std::vector<GRBConstr>>>> m_flowConservationConstraints;
    std::vector<GRBConstr> m_linkCapacityConstraints;
    std::vector<GRBConstr> m_cpuCapacityConstraints;
    std::vector<GRBConstr> m_storageCapacityConstraints;
    std::vector<std::vector<GRBConstr>> m_serviceDelayConstraints;
    std::vector<std::vector<std::vector<GRBConstr>>> m_vnfDelayConstraints;
    GRBConstr m_maxBandwidthCut;

  public:
    CompactILP(const Instance& _inst, GRBEnv& _env);

    const auto& getModel() const { return m_model; }
    auto& getModel() { return m_model; }

    const auto& getLinkCapacityConstraints() const { return m_linkCapacityConstraints; }
    const auto& getCPUCapacityConstraints() const { return m_cpuCapacityConstraints; }
    const auto& getStorageCapacityConstraints() const { return m_storageCapacityConstraints; }

    void setVarType(char _type);

    /*
     * Returns the given solution, only if the model has been solved as a ILP
     */
    std::optional<Solution> buildSolution(const Instance& _inst) const;
};

class ExtendedFormulation {
    struct Column
    {
        GRBVar var;
        Embedding embedding;
    };

    GRBModel m_model;

    // Columns
    std::vector<std::vector<Column>> m_columns;

    // Constraints
    std::vector<GRBConstr> m_embeddingConstraints;
    std::vector<GRBConstr> m_linkCapacityConstraints;
    std::vector<GRBConstr> m_cpuCapacityConstraints;
    std::vector<GRBConstr> m_storageCapacityConstraints;

  public:
    struct DualValues
    {
        std::vector<double> embeddingDuals;
        std::vector<double> linkCapacityDuals;
        std::vector<double> cpuCapacityDuals;
        std::vector<double> storageCapacityDuals;
    };
    ExtendedFormulation(const Instance& _inst, GRBEnv& _env);

    DualValues getDualValues() const;

    const auto& getModel() const { return m_model; }
    auto& getModel() { return m_model; }

    void addColumn(const Embedding& _emb, const Instance& _inst);

    const auto& getColumns() const { return m_columns; }

    const auto& getEmbeddingConstraints() const { return m_embeddingConstraints; }
    const auto& getLinkCapacityConstraints() const { return m_linkCapacityConstraints; }
    const auto& getCPUCapacityConstraints() const { return m_cpuCapacityConstraints; }
    const auto& getStorageCapacityConstraints() const { return m_storageCapacityConstraints; }

    void setColumnType(char _type);
    std::optional<Solution> buildSolution(const Instance& _inst) const;
};

double getReducedCost(const ExtendedFormulation::DualValues& _duals,
  const Embedding& _col,
  const Instance& _inst);

class EmbeddingILP {
    PhysicalNetwork m_physicalNetwork;
    Service m_service;

    std::unique_ptr<GRBModel> m_model;

    // Variables
    std::vector<std::vector<GRBVar>> m_flowVars;
    std::vector<std::vector<GRBVar>> m_vnfVars;

    // Objective function

    GRBLinExpr m_obj;

    // Constraints
    std::vector<GRBConstr> m_vnfToGraphConstraints;
    std::vector<GRBConstr> m_oneFunctionPerNodeConstraints;
    std::vector<std::vector<GRBConstr>> m_flowConservationConstraints;
    GRBConstr m_serviceDelayConstraints;
    std::vector<GRBConstr> m_vnfDelayConstraints;

  public:
    EmbeddingILP(const Instance& _inst, Service _service, GRBEnv& _env);


    void setObjectiveCoefficient(const std::vector<double>& _flowCoeff,
      const std::vector<double>& _cpuCoeff,
      const std::vector<double>& _storageCoeff);

    Embedding getEmbedding(std::size_t _serviceId) const;
    std::vector<Embedding> getEmbeddings(std::size_t _serviceId);
    void setServiceDelay(double _val);

    const auto& getModel() const { return *m_model; }
    auto& getModel() { return *m_model; }
};


}// namespace VNE

#endif
