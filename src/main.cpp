#include "models.hpp"
#include "algorithm.hpp"
#include "vne.hpp"
#include "spdlog/spdlog.h"
#include <boost/graph/detail/adjacency_list.hpp>
#include <nlohmann/json.hpp>
#include <boost/program_options.hpp>
#include <fmt/core.h>
#include <nlohmann/json_fwd.hpp>
#include <gurobi_c++.h>
#include <iostream>
#include <numeric>
#include <random>
#include <cstdio>
#include "spdlog/cfg/env.h"
#include <fstream>


enum class Algorithm {
    LP,
    ILP,
    CGRR,
    CGBnB,
};

std::istream& operator>>(std::istream& in, Algorithm& _algorithm) {
    std::string token;
    in >> token;
    if (token == "ILP") {
        _algorithm = Algorithm::ILP;
    } else if (token == "LP") {
        _algorithm = Algorithm::LP;
    } else if (token == "CGBnB") {
        _algorithm = Algorithm::CGBnB;
    } else if (token == "CGRR") {
        _algorithm = Algorithm::CGRR;
    } else {
        in.setstate(std::ios_base::failbit);
    }
    return in;
}

std::string getAlgorithmName(Algorithm _algo) {
    switch (_algo) {
    case Algorithm::LP:
        return "LP";
    case Algorithm::ILP:
        return "ILP";
    case Algorithm::CGRR:
        return "CGRR";
    case Algorithm::CGBnB:
        return "CGBnB";
    }
}

namespace po = boost::program_options;
class AllowedOptions {
    po::options_description m_desc{ "Allowed options" };
    po::variables_map m_vm;
    po::positional_options_description m_p;
    double m_physCapa{ 0.0 };
    double m_physDelay{ 0.0 };
    double m_physCPU{ 0.0 };
    double m_physStorage{ 0.0 };
    // Requirements
    double m_maxBwReq{ 0.0 };
    double m_cpuReq{ 0.0 };
    double m_storageReq{ 0.0 };
    double m_delayReq{ 0.0 };
    double m_serviceDelayReq{ 0.0 };
    std::string m_instanceFilename{ "" };
    std::string m_type{ "" };
    std::size_t m_nbServices{ std::numeric_limits<std::size_t>::max() };
    std::size_t m_vnfSize{ std::numeric_limits<std::size_t>::max() };
    std::size_t m_seed{ std::numeric_limits<std::size_t>::max() };
    Algorithm m_algorithm{ Algorithm::ILP };
    std::string m_outputFilename{ "" };

  public:
    AllowedOptions() {
        m_desc.add_options()("help", "Produces help message");
        // Mandatory parameters
        m_desc.add_options()(
          "instance", po::value<std::string>(&m_instanceFilename), "Instance name");
        m_desc.add_options()("algorithm", po::value<Algorithm>(&m_algorithm), "Algorithm to run");
        m_desc.add_options()("type", po::value<std::string>(&m_type), "Service type");
        m_desc.add_options()("seed", po::value<std::size_t>(&m_seed), "Seed");
        m_desc.add_options()("nb", po::value<std::size_t>(&m_nbServices), "Number of services");
        m_desc.add_options()("vnf-size", po::value<std::size_t>(&m_vnfSize), "Size of the chains");
        m_p.add("instance", 1).add("algorithm", 1).add("type", 1).add("nb", 1).add("vnf-size", 1);
        // Optional parameters
        m_desc.add_options()("output",
          po::value<std::string>(&m_outputFilename)->default_value(""),
          "Name of the output filename");
        m_desc.add_options()("physCapa",
          po::value<double>(&m_physCapa)->default_value(1000),
          "Physical link capacity");
        m_desc.add_options()(
          "physDelay", po::value<double>(&m_physDelay)->default_value(1), "Physical link delay");
        m_desc.add_options()(
          "physCPU", po::value<double>(&m_physCPU)->default_value(1000), "Physical CPU capacity");
        m_desc.add_options()("physStorage",
          po::value<double>(&m_physStorage)->default_value(1000),
          "Physical storage capacity");
        m_desc.add_options()(
          "maxBwReq", po::value<double>(&m_maxBwReq)->default_value(1), "Bandwidth requirements");
        m_desc.add_options()(
          "storageReq", po::value<double>(&m_storageReq)->default_value(1), "Storage requirements");
        m_desc.add_options()(
          "cpuReq", po::value<double>(&m_cpuReq)->default_value(1), "CPU requirements");
        m_desc.add_options()("delayReq",
          po::value<double>(&m_delayReq)->default_value(1000),
          "Delay requirements between functions");
        m_desc.add_options()("serviceDelayReq",
          po::value<double>(&m_serviceDelayReq)->default_value(1000),
          "Delay requirements for the whole service");
    }

    bool parse(int argc, char** argv) {
        po::store(po::command_line_parser(argc, argv).options(m_desc).positional(m_p).run(), m_vm);
        po::notify(m_vm);
        if (m_vm.count("help")) {
            std::cout << m_desc << "";
            return false;
        }
        return true;
    }

    const auto& getPhysicalCapacity() const { return m_physCapa; }
    const auto& getPhysicalDelay() const { return m_physDelay; }
    const auto& getPhysicalCPU() const { return m_physCPU; }
    const auto& getPhysicalStorage() const { return m_physStorage; }
    const auto& getBwRequirements() const { return m_maxBwReq; }
    const auto& getCPURequirements() const { return m_cpuReq; }
    const auto& getStorageRequirements() const { return m_cpuReq; }
    const auto& getDelayRequirements() const { return m_delayReq; }
    const auto& getInstanceFilename() const { return m_instanceFilename; }
    const auto& getVNFType() const { return m_type; }
    const auto& getNbServices() const { return m_nbServices; }
    const auto& getVNFSize() const { return m_vnfSize; }
    const auto& getServiceDelayRequirements() const { return m_serviceDelayReq; }
    const auto& getSeed() const { return m_seed; }
    const auto& getAlgorithm() const { return m_algorithm; }
    const auto& getOutputFilename() const { return m_outputFilename; }
};

int main(int argc, char** argv) {
    spdlog::cfg::load_env_levels();
    AllowedOptions args;
    if (!args.parse(argc, argv)) {
        return 1;
    }

    spdlog::info(fmt::format("Loading {}", args.getInstanceFilename()));
    const VNE::Instance inst =
      VNE::loadInstance(nlohmann::json::parse(std::ifstream(args.getInstanceFilename())));
    spdlog::info("Network size: {} nodes, {} edges",
      boost::num_vertices(inst.getPhysicalNetwork()),
      num_edges(inst.getPhysicalNetwork()));
    for (const auto& service : inst.getServices()) {
        spdlog::info("{} services size: {} nodes, {} edges",
          service.nbRequested,
          boost::num_vertices(service.network),
          num_edges(service.network));
    }
    std::seed_seq seed{ { args.getSeed() } };
    std::mt19937 gen(seed);
    GRBEnv env;
    nlohmann::json outputInformations;
    outputInformations["algorithm"] = getAlgorithmName(args.getAlgorithm());
    outputInformations["services"] = args.getVNFType();
    try {
        const auto start = std::chrono::high_resolution_clock::now();
        auto sol = [&]() -> std::pair<std::optional<VNE::Solution>, double> {
            switch (args.getAlgorithm()) {
            case Algorithm::ILP: {
                VNE::CompactILP ilp(inst, env);
                ilp.getModel().optimize();
                return { ilp.buildSolution(inst), ilp.getModel().get(GRB_DoubleAttr_ObjBound) };
            }
            case Algorithm::LP: {
                VNE::CompactILP ilp(inst, env);
                ilp.setVarType(GRB_CONTINUOUS);
                ilp.getModel().optimize();
                return { std::nullopt, ilp.getModel().get(GRB_DoubleAttr_ObjVal) };
            }
            case Algorithm::CGRR:
                return cgAndRandomizedRounding(inst, env, gen);
            case Algorithm::CGBnB: {
                return cgAndBranchAndBound(inst, env);
            }
            default:
                return { std::nullopt, std::numeric_limits<double>::infinity() };
            }
        }();
        const auto end = std::chrono::high_resolution_clock::now();
        const auto runningTime = std::chrono::duration<double>(end - start).count();
        outputInformations["runningTime"] = runningTime;
        outputInformations["upperBound"] = sol.second;
        outputInformations["embeddings"] = nlohmann::json::array();
        for (const auto& embedding : sol.first->embeddings) {
            outputInformations["embeddings"].emplace_back(
              nlohmann::json::object({ { "serviceId", embedding->serviceId },
                { "edges", embedding->edges },
                { "nodes", embedding->vertices } }));
        }
        if (!sol.first) {
            spdlog::info("No solution found after {} seconds", runningTime);
            return 0;
        }
        outputInformations["objValue"] = std::accumulate(sol.first->embeddings.begin(),
          sol.first->embeddings.end(),
          0,
          [](const auto _acc, const auto& _emb) { return _acc + _emb.has_value(); });
        spdlog::info("Found solution after {} seconds", runningTime);
        for (const auto& emb : sol.first->embeddings) {
            fmt::print("{}ms, ", getEndToEndLatency(inst, *emb));
        }
    } catch (const GRBException& e) {
        spdlog::error("GUROBI error {}: {}", e.getErrorCode(), e.getMessage());
    }
    if (!args.getOutputFilename().empty()) {
        std::ofstream ofs(args.getOutputFilename());
        ofs << outputInformations;
    }
    return 0;
}
