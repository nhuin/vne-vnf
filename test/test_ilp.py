import vne_vnf
import networkx as nx


def generate_same_services(virtual_network, nb_requested, req_cpu, req_storage, req_bandwidth, req_delay, req_service_delay):
    cpu_requirements = [req_cpu for _ in range(virtual_network.order())]
    storage_requirements = [
        req_storage for _ in range(virtual_network.order())]
    bw_requirements = [req_bandwidth for _ in range(virtual_network.size())]
    delay_requirements = [req_delay for _ in range(virtual_network.size())]

    return [vne_vnf.Service(network=virtual_network, fixedLocations=[[] for f in range(virtual_network.order())], CPU_requirements=cpu_requirements,
                            storage_requirements=storage_requirements, bandwidth_requirements=bw_requirements,
                            delay_requirements=delay_requirements, service_delay=req_service_delay, nb_requested=nb_requested)]


def generate_instance(network, services, capa_cpu, capa_storage, capa_delay, capa_bandwidth):
    cpu_capacities = [capa_cpu for _ in range(network.order())]
    storage_capacities = [capa_storage for _ in range(network.order())]
    delays = [capa_delay for _ in range(network.size())]
    bws = [capa_bandwidth for _ in range(network.size())]
    physical_network = vne_vnf.PhysicalNetwork(
        [(int(u), int(v)) for u, v in network.edges()], network.order())
    return vne_vnf.Instance(network=physical_network,
                            CPU_capacities=cpu_capacities, storage_capacities=storage_capacities,
                            delays=delays, bandwidths=bws, services=services)


class ParamVnf:
    nb_requested = 10
    req_cpu = 1
    req_storage = 1
    req_bandwidth = 1
    req_delay = 1000
    req_service_delay = 1000


virtual_network = vne_vnf.VirtualNetwork([(0, 1), (1, 2)], 3)

services = generate_same_services(virtual_network, ParamVnf.nb_requested, ParamVnf.req_cpu,
                                  ParamVnf.req_storage, ParamVnf.req_bandwidth, ParamVnf.req_delay, ParamVnf.req_service_delay)

network = nx.DiGraph(nx.read_graphml("BtEurope.graphml"))

instance = generate_instance(
    network, services, capa_cpu=10, capa_storage=10, capa_delay=1, capa_bandwidth=10)

env = vne_vnf.GRBEnv()

optimal, solution = vne_vnf.ilp(instance, env, True, 0.1)

print("Nb placés :", sum(1 for emb in solution.embeddings if emb),
      "isOptimal: ", optimal)
for emb in solution.embeddings:
    if emb:
        print(emb.vertices)
        print([[list(network.edges())[e] for e in edges]
              for edges in emb.edges])
