FROM gurobi/optimizer:11.0.3 AS gurobi

ENV DEBIAN_FRONTEND=noninteractive
ENV PATH="/venv/bin:${PATH}"

# Use mounts to avoid redownloading during dev, and probably avoid bloating the image ?
RUN --mount=target=/var/lib/apt/lists,type=cache,sharing=locked --mount=target=/var/cache/apt,type=cache,sharing=locked <<EOF
    apt update
    apt install -y python3-virtualenv git wget lsb-release software-properties-common gnupg
    wget https://apt.llvm.org/llvm.sh && chmod +x llvm.sh && ./llvm.sh 18
EOF

# Use venv because debian doesn't like pip
RUN --mount=target=/venv,type=cache <<EOF
    virtualenv /venv
    pip3 install conan cmake
EOF

# Setup CppRO
ARG CACHEBUST=2
RUN --mount=type=cache,target=/root/.conan2 --mount=target=/venv,type=cache <<EOF
    conan profile detect -e
    git clone https://gitlab.inria.fr/nhuin/CppRO.git /CppRO
    conan create /CppRO --build=missing -s compiler.cppstd=20
EOF

ENV GUROBI_HOME=/opt/gurobi/linux
COPY . /src
RUN --mount=type=cache,target=/root/.conan2 --mount=target=/venv,type=cache <<EOF
    cmake -S /src -B /build -DCMAKE_BUILD_RELEASE=Release -DENABLE_DEVELOPER_MODE=OFF -DCMAKE_FIND_DEBUG_MODE=ON
    cmake --build /build/ -j
    cmake --install /build
EOF
