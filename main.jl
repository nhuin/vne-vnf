using Graphs
using GraphIO
using EzXML
using JuMP, BlockDecomposition, Coluna, Gurobi
using ArgParse

#mutable struct Service
#    graph::DiGraph
#    link_requirements::Dict{Edge, Real}
#    cpu_requirements::Dict{Vertex, Real}
#end

#mutable struct Instance
#   # Network
#    network::DiGraph
#    link_capacities::Dict{Edge, Real}
#    nodes_capacities::Dict{Vertex, Real}
#    # Services
#    services::Array{Service, 1}
#end

function build_compact_model(network, virtual_graphs, link_capacities, node_capacities, link_requirements, cpu_requirements)
    model = Model(Gurobi.Optimizer)
    K = 1:length(virtual_graphs)
    return build_model(model, network, virtual_graphs, link_capacities, node_capacities, link_requirements, cpu_requirements, K)
end

function build_model(model, network, virtual_graphs, link_capacities, node_capacities, link_requirements, cpu_requirements, K)
    E = edges(network)
    V = vertices(network)
    @variable(model, x[k in K, e in E, l in edges(virtual_graphs[k])], Bin)
    @variable(model, y[k in K, v in V, f in vertices(virtual_graphs[k])], Bin)
    @constraint(model, oneFunction[k in K, f in vertices(virtual_graphs[k])], sum(y[k, v, f] for v in V) <= 1)
    @constraint(model, oneNode[k in K, v in V], sum(y[k, v, f] for f in vertices(virtual_graphs[k])) <= 1)
    @constraint(model, flowConservation[k in K, v in V, l in edges(virtual_graphs[k])], sum(x[k, Edge(v, w), l] for w in outneighbors(network, v)) - sum(x[k, Edge(u, v), l] for u in inneighbors(network, v)) + y[k, v, src(l)] - y[k, v, dst(l)] == 0)
    @constraint(model, linkCapacities[e in E], sum(link_requirements[k, l] * x[k, e, l] for k in K for l in edges(virtual_graphs[k])) <= link_capacities[e]) 
    @constraint(model, cpuCapacities[v in V], sum(cpu_requirements[k, f] * y[k, v, f] for k in K for f in vertices(virtual_graphs[k])) <= node_capacities[v]) 
    @objective(model, Max, sum(y[k, v, 1] for k in K for v in V))
    return x, y, model
end

function build_coluna_model(network, virtual_graphs, link_capacities, node_capacities, link_requirements, cpu_requirements)
    coluna = optimizer_with_attributes(
        Coluna.Optimizer,
        "params" => Coluna.Params(
            solver = Coluna.Algorithm.TreeSearchAlgorithm() # default BCP
        ),
        "default_optimizer" => Gurobi.Optimizer # GLPK for the master & the subproblems
    )
    @axis(K, 1:length(virtual_graphs))
    model = BlockModel(coluna)
    x, y, model = build_model(model, network, virtual_graphs, link_capacities, node_capacities, link_requirements, cpu_requirements, K) 
    @dantzig_wolfe_decomposition(model, decomposition, K)
    println(decomposition)
    return x, y, model
end

function main()
	parsed_args = parse_args(ARGS, s)

    g = loadgraph(parsed_args["graph_filename"], "graph", GraphIO.GraphMLFormat())
    network = DiGraph(nv(g))
    for e in edges(g)
        u = src(e)
        v = dst(e)
        add_edge!(network, u, v)
        add_edge!(network, v, u)
    end

    println("Loaded ", parsed_args["graph_filename"])
    println(network)

    link_capacities = Dict( e => parsed_args["link_capa"] for e in edges(network) )
    node_capacities = Dict( v => parsed_args["node_capa"] for v in vertices(network) )

    virtual_graphs = [get_daisy_chain(6) for i in 1:parsed_args["nb_services"]]
    link_requirements = Dict( (k, l) => parsed_args["link_req"] for k in 1:length(virtual_graphs) for l in edges(virtual_graphs[k]) )
    cpu_requirements = Dict( (k, v) => parsed_args["cpu_req"] for k in 1:length(virtual_graphs) for v in vertices(virtual_graphs[k]) )

    if parsed_args["algorithm"] == "compact"
        x, y, model = build_compact_model(network, virtual_graphs, link_capacities, node_capacities, link_requirements, cpu_requirements)
        optimize!(model)
    else
        x, y, model = build_coluna_model(network, virtual_graphs, link_capacities, node_capacities, link_requirements, cpu_requirements)
        optimize!(model)
    end
end

function get_daisy_chain(size)
    digraph = DiGraph(size)
    for i in 1:size-1
        add_edge!(digraph, i, i+1)
        add_edge!(digraph, i+1, i)
    end
    return digraph
end
