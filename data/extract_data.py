import pandas as pd
import json
import sqlite3


if __name__ == "__main__":
    con = sqlite3.connect("runs.db")
    cur = con.cursor()
    df = pd.read_sql_query("SELECT * from run;", con)
    df2 = pd.json_normalize(df['config'].apply(json.loads))
    df[df2.columns] = df2
    df[['service_graph_type', 'chain_size']
       ] = df['service_graph_type'].str.split('_', expand=True)
    comp_df = df.pivot_table(columns='algorithm', index=[
                             'capa_cpu', 'chain_size', ], values='result')
    print(comp_df)
    comp_df.to_csv("data/node_constraints.csv")
