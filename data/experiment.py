import one_service_type
import itertools


def generate_node_resources_configs():
    configs = []
    for resource, chain_size, algo in itertools.product([10, 20, 40], range(3, 9), ['ILP', 'CGBnB']):
        configs.append({
            "networkFilename": 'data/BtEurope.graphml',
            "service_graph_type": f"chain_{chain_size}",
            "capa_cpu": resource,
            "capa_storage": resource,
            "algorithm": algo
        })
        return configs


def generate_daisy_limits_configs():
    configs = []
    for chain_size, algo in itertools.product(range(3, 9), ['CGBnB']):
        configs.append({
            "networkFilename": 'data/BtEurope.graphml',
            "service_graph_type": f"chain_{chain_size}",
            "algorithm": algo,
            "capa_bandwidth": 10
        })
    return configs


if "__main__" == __name__:
    configs = generate_daisy_limits_configs()

    one_service_type.ex.observers.append(
        one_service_type.MyObserver(len(configs)))
    for conf in configs:
        one_service_type.ex.run(config_updates=conf)
