import sacred
import vne_vnf
import networkx as nx
import requests
import pandas as pd


class MyObserver(sacred.observers.RunObserver):
    count = 0
    url = 'https://mattermost.imt-atlantique.fr/hooks/7kaomphni3f9zcy7oyq54c8jqc'

    def __init__(self, nb_total_run):
        self.nb_total_run = nb_total_run

    def queued_event(self, ex_info, command, host_info, queue_time, config, meta_info,
                     _id):
        print(f"Queued {ex_info}")
        pass

    def started_event(self, ex_info, command, host_info, start_time,
                      config, meta_info, _id):
        self.count += 1
        self.current_run = {
            'config': config,
            'meta_info': meta_info}
        pass

    def heartbeat_event(self, info, captured_out, beat_time, result):
        pass

    def completed_event(self, stop_time, result):
        x = requests.post(
            self.url, json={'text': f"Finished {self.count}/{self.nb_total_run} run(s)"})
        print(x.text)
        pass

    def interrupted_event(self, interrupt_time, status):
        pass

    def failed_event(self, fail_time, fail_trace):
        pass

    def resource_event(self, filename):
        print(f"Used file: {filename}")

    def artifact_event(self, name, filename):
        pass


ex = sacred.Experiment()
ex.observers.append(sacred.observers.SqlObserver('sqlite:///runs.db'))


@ ex.config
def default_config():
    algorithm = 'CGBnB'
    nb_requested = 200
    req_cpu = 1
    req_storage = 1
    req_bandwidth = 1
    req_delay = 1000
    req_service_delay = 1000
    capa_cpu = 1000
    capa_storage = 1000
    capa_delay = 1
    capa_bandwidth = 1000


@ ex.capture
def generate_same_services(virtual_network, nb_requested, req_cpu, req_storage, req_bandwidth, req_delay, req_service_delay):
    cpu_requirements = [req_cpu for _ in range(virtual_network.order())]
    storage_requirements = [
        req_storage for _ in range(virtual_network.order())]
    bw_requirements = [req_bandwidth for _ in range(virtual_network.size())]
    delay_requirements = [req_delay for _ in range(virtual_network.size())]

    return [vne_vnf.Service(network=virtual_network, CPU_requirements=cpu_requirements,
                            storage_requirements=storage_requirements, bandwidth_requirements=bw_requirements,
                            delay_requirements=delay_requirements, service_delay=req_service_delay, nb_requested=nb_requested)]


@ ex.capture
def generate_instance(network, services, capa_cpu, capa_storage, capa_delay, capa_bandwidth):
    cpu_capacities = [capa_cpu for _ in range(network.order())]
    storage_capacities = [capa_storage for _ in range(network.order())]
    delays = [capa_delay for _ in range(network.size())]
    bws = [capa_bandwidth for _ in range(network.size())]
    physical_network = vne_vnf.PhysicalNetwork(
        [(int(u), int(v)) for u, v in network.edges()], network.order())
    return vne_vnf.Instance(network=physical_network,
                            CPU_capacities=cpu_capacities, storage_capacities=storage_capacities,
                            delays=delays, bandwidths=bws, services=services)


@ ex.capture
def cgSameService(networkFilename, service_graph_type, algorithm, _run):
    # Create services
    service_graph_size = int(service_graph_type.split("_")[1])
    if service_graph_type.startswith('ring'):
        graph = vne_vnf.createRing(service_graph_size)
    elif service_graph_type.startswith('chain'):
        graph = vne_vnf.createChain(service_graph_size)
    elif service_graph_type.startswith('star'):
        graph = vne_vnf.createStar(service_graph_size)
    services = generate_same_services(graph)
    network = nx.DiGraph(nx.read_graphml(ex.open_resource(networkFilename)))
    print(
        f"Loaded {networkFilename} with {network.order()} nodes and {network.size()} edges")
    instance = generate_instance(network, services)
    env = vne_vnf.GRBEnv()
    if algorithm == 'CGBnB':
        sol, upper_bound = vne_vnf.cgAndBranchAndBound(instance, env)
    elif algorithm == "ILP":
        sol, upper_bound = vne_vnf.ilp(instance, env)
    _run.log_scalar('upper_bound', upper_bound)
    return sol


@ ex.main
def main():
    solution = cgSameService()
    return sum(1 for emb in solution.embeddings if emb)


if __name__ == "__main__":
    ex.run_commandline()
