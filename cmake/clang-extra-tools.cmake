if(CMAKE_VERSION VERSION_GREATER 3.6)
    # Add clang-tidy if available
    option(CLANG_TIDY "Enable Clang-Tidy" OFF)
    option(CLANG_TIDY_FIX "Perform fixes for Clang-Tidy" OFF)
    if(CLANG_TIDY)
        find_program(
            CLANG_TIDY_EXE
            NAMES "clang-tidy"
            DOC "Path to clang-tidy executable"
        )

        if(CLANG_TIDY_EXE)
            if(CLANG_TIDY_FIX)
                set(CMAKE_CXX_CLANG_TIDY "${CLANG_TIDY_EXE}" "-fix")
            else()
                set(CMAKE_CXX_CLANG_TIDY "${CLANG_TIDY_EXE}")
            endif()
        endif()
    endif()
endif()